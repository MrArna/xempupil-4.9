#!/usr/bin/env python

#####################################################################
# xempowermon
# -----------
# A power profiler for Xen domains.
# Author: Matteo Ferroni <matteo.ferroni@polimi.it>
#
# Based on: xenmon
# xenmon is a front-end for xempowerbaked.
# There is a curses interface for live monitoring. XenMon also allows
# logging to a file. For options, run python xenmon.py -h
#
# Copyright (C) 2005,2006 by Hewlett Packard, Palo Alto and Fort Collins
# Authors: Lucy Cherkasova, lucy.cherkasova@hp.com
#          Rob Gardner, rob.gardner@hp.com
#          Diwaker Gupta, diwaker.gupta@hp.com
#####################################################################
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; under version 2 of the License.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#####################################################################

import mmap
import struct
import os
import time
import optparse as _o
import curses as _c
import math
import sys

#ADD FOR ARNABOLDI
LOG_FILE = "/home/teof/domain.log"

# constants
NSAMPLES = 100
NDOMAINS = 32
IDLE_DOMAIN = -1    # idle domain's ID
PMC_COUNT = 4       # pmc0, pmc1, pmc2, pmc3
RAPL_COUNT = 4      # pkg, pp0, pp1, dram

# the struct strings for qos_info
# Format  C Type
# x       pad byte
# c       char
# b       signed
# B       unsigned
# ?       _Bool
# h       short
# H       unsigned short
# i       int
# I       unsigned int
# l       long
# L       unsigned long
# q       long long
# Q       unsigned long long
# f       float
# d       double
# s       char[]
# p       char[]
# P       void *
ST_DOM_INFO = "6Q3i2H32s"                       # TODO: export 32 -> MAX_NAME_SIZE 
ST_QDATA = "%dQ" % (6*NDOMAINS + 4)
ST_QDATA_POWER = "%dQ" % (PMC_COUNT*NDOMAINS + 3 + PMC_COUNT + 2*RAPL_COUNT)

# size of mmaped files
QOS_DATA_SIZE = struct.calcsize(ST_QDATA)*NSAMPLES + struct.calcsize(ST_DOM_INFO)*NDOMAINS + struct.calcsize("4i")
POWER_DATA_SIZE = struct.calcsize(ST_QDATA_POWER)*NSAMPLES + struct.calcsize("2i1d")


# NOTE: this has to be coherent with: xempowerbaked.SHARED_MEM_FILE 
# location of mmaped file, hard coded right now
SHM_FILE = "/var/run/xenq-qos-shm"
SHM_FILE_POWER = "/var/run/xenq-power-shm"


# format strings
TOTALS = 15*' ' + "%6.2f%%" + 35*' ' + "%6.2f%%"

ALLOCATED = "Allocated"
GOTTEN = "Gotten"
BLOCKED = "Blocked"
WAITED = "Waited"
IOCOUNT = "I/O Count"
EXCOUNT = "Execution count"
PMC0 = "UOPS_RETIRED.ALL"
PMC1 = "UOPS_ISSUED.ANY"
PMC2 = "UOPS_ISSUED.ANY-stalls"
PMC3 = "RESOURCE_STALLS.ANY"

# globals
dom_in_use = []
energy_unit = None
msr_command = "modprobe msr; chmod -R 666 /dev/cpu/*/msr"

# our curses screen
stdscr = None

# parsed options
options, args = None, None

# TODO: remove unused cmdlines here
# the optparse module is quite smart
# to see help, just run xempowermon -h
def setup_cmdline_parser():
    parser = _o.OptionParser()
    parser.add_option("-l", "--live", dest="live", action="store_true",
                      default=True, help = "show the ncurses live monitoring frontend (default)")
    parser.add_option("-n", "--notlive", dest="live", action="store_false",
                      default="True", help = "write to file instead of live monitoring")
    parser.add_option("-p", "--prefix", dest="prefix",
                      default = "log", help="prefix to use for output files")
    parser.add_option("-t", "--time", dest="duration",
            action="store", type="int", default=10, 
            help="stop logging to file after this much time has elapsed (in seconds). set to 0 to keep logging indefinitely")
    parser.add_option("-i", "--interval", dest="interval",
            action="store", type="int", default=1000,
            help="interval for logging (in ms)")
    parser.add_option("--ms_per_sample", dest="mspersample",
            action="store", type="int", default=100,
            help = "determines how many ms worth of data goes in a sample")
    parser.add_option("--cpu", dest="cpu", action="store", type="int", default=0,
            help = "specifies which cpu to display data for")

    parser.add_option("--allocated", dest="allocated", action="store_true",
                      default=False, help="Display allocated time for each domain")
    parser.add_option("--noallocated", dest="allocated", action="store_false",
                      default=False, help="Don't display allocated time for each domain")

    parser.add_option("--blocked", dest="blocked", action="store_true",
                      default=True, help="Display blocked time for each domain")
    parser.add_option("--noblocked", dest="blocked", action="store_false",
                      default=True, help="Don't display blocked time for each domain")

    parser.add_option("--waited", dest="waited", action="store_true",
                      default=True, help="Display waiting time for each domain")
    parser.add_option("--nowaited", dest="waited", action="store_false",
                      default=True, help="Don't display waiting time for each domain")

    parser.add_option("--excount", dest="excount", action="store_true",
                      default=False, help="Display execution count for each domain")
    parser.add_option("--noexcount", dest="excount", action="store_false",
                      default=False, help="Don't display execution count for each domain")
    parser.add_option("--iocount", dest="iocount", action="store_true",
                      default=False, help="Display I/O count for each domain")
    parser.add_option("--noiocount", dest="iocount", action="store_false",
                      default=False, help="Don't display I/O count for each domain")
    parser.add_option("--pmc0", dest="pmc0", action="store_true",
                      default=True, help="Display " + PMC0 + " for each domain")
    parser.add_option("--pmc1", dest="pmc1", action="store_true",
                      default=False, help="Display " + PMC1 + " for each domain")
    parser.add_option("--pmc2", dest="pmc2", action="store_true",
                      default=False, help="Display " + PMC2 + " for each domain")
    parser.add_option("--pmc3", dest="pmc3", action="store_true",
                      default=False, help="Display " + PMC3 + " for each domain")

    # TODO: add here command to specify which RAPL counter to read

    return parser

# encapsulate information about a domain
class DomainInfo:
    def __init__(self):
        self.allocated_sum = 0
        self.gotten_sum = 0
        self.blocked_sum = 0
        self.waited_sum = 0
        self.exec_count = 0     # Number of executions of the domain (i.e., context switches that involve that domain)
        self.iocount_sum = 0
        self.ffp_samples = []

    def gotten_stats(self, passed):
        total = float(self.gotten_sum)
        per = 100*total/passed
        exs = self.exec_count
        if exs > 0:
            avg = total/exs
        else:
            avg = 0
        return [total/(float(passed)/10**9), per, avg]

    def waited_stats(self, passed):
        total = float(self.waited_sum)
        per = 100*total/passed
        exs = self.exec_count
        if exs > 0:
            avg = total/exs
        else:
            avg = 0
        return [total/(float(passed)/10**9), per, avg]

    def blocked_stats(self, passed):
        total = float(self.blocked_sum)
        per = 100*total/passed
        ios = self.iocount_sum
        if ios > 0:
            avg = total/float(ios)
        else:
            avg = 0
        return [total/(float(passed)/10**9), per, avg]

    def allocated_stats(self, passed):
        total = self.allocated_sum
        exs = self.exec_count
        if exs > 0:
            return float(total)/exs
        else:
            return 0

    def ec_stats(self, passed):
        total = float(self.exec_count/(float(passed)/10**9))
        return total

    def io_stats(self, passed):
        total = float(self.iocount_sum)
        exs = self.exec_count
        if exs > 0:
            avg = total/exs
        else:
            avg = 0
        return [total/(float(passed)/10**9), avg]

    def stats(self, passed):
        return [self.gotten_stats(passed), self.allocated_stats(passed), self.blocked_stats(passed), 
                self.waited_stats(passed), self.ec_stats(passed), self.io_stats(passed)]


# encapsulate information about the socket
class SocketData:
    def __init__(self):
        self.total = Stats()
        self.domain = {}
        self.s_passed = 0
        for i in range(0, NDOMAINS):
            self.domain[i] = Stats()

class Stats:
    def __init__(self):
        self.energy = ConsumptionData()
        self.power = ConsumptionData()
        self.performance = PerformanceData()
        self.performance_rel = PerformanceData()

    # Return relative load in the last period  
    def __update_relative_perf(self, totalPerformance):
        self.performance_rel.pmc0 = float(self.performance.pmc0)/totalPerformance.pmc0
        self.performance_rel.pmc1 = float(self.performance.pmc1)/totalPerformance.pmc1
        self.performance_rel.pmc2 = float(self.performance.pmc2)/totalPerformance.pmc2
        self.performance_rel.pmc3 = float(self.performance.pmc3)/totalPerformance.pmc3

    # Return energy accounted in the last period  
    def __update_energy(self, totalConsumption):
        self.energy.pkg = self.__split_contribution_on(totalConsumption.pkg)
        self.energy.pp0 = self.__split_contribution_on(totalConsumption.pp0)
        self.energy.pp1 = self.__split_contribution_on(totalConsumption.pp1)
        self.energy.dram = self.__split_contribution_on(totalConsumption.dram)

    # Return power accounted in the last period  
    def __update_power(self, s_passed):
        self.power.pkg = self.energy.pkg/s_passed
        self.power.pp0 = self.energy.pp0/s_passed
        self.power.pp1 = self.energy.pp1/s_passed
        self.power.dram = self.energy.dram/s_passed

    def __split_contribution_on(self, energy):
        # TODO: define here how to split energy_total and power_total consumption
        # Current solution: scale using relative PMC0 counts
        return energy*self.performance_rel.pmc0

    def update_stats(self, totalPerformance, totalConsumption, s_passed):
        self.__update_relative_perf(totalPerformance)
        self.__update_energy(totalConsumption)
        self.__update_power(s_passed)

# Store power accounted to the  in the last period  
class ConsumptionData:
    def __init__(self):
        self.pkg = 0
        self.pp0 = 0
        self.pp1 = 0
        self.dram = 0

# Store events counted in the last period  
class PerformanceData:
    def __init__(self):
        self.pmc0 = 0
        self.pmc1 = 0
        self.pmc2 = 0
        self.pmc3 = 0



# report values over desired interval
def summarize(startat, endat, duration, qos_samples, power_samples):
    
    global energy_unit

    dominfos = {}
    for i in range(0, NDOMAINS):
        dominfos[i] = DomainInfo()
        
    qos_passed = 1              # to prevent zero division
    curid = startat
    numbuckets = 0
    lost_samples = []
    ffp_samples = []
    
    while qos_passed < duration:
        for i in range(0, NDOMAINS):
            if dom_in_use[i]:
                dominfos[i].gotten_sum += qos_samples[curid][0*NDOMAINS + i]
                dominfos[i].allocated_sum += qos_samples[curid][1*NDOMAINS + i]
                dominfos[i].waited_sum += qos_samples[curid][2*NDOMAINS + i]
                dominfos[i].blocked_sum += qos_samples[curid][3*NDOMAINS + i]
                dominfos[i].exec_count += qos_samples[curid][4*NDOMAINS + i]
                dominfos[i].iocount_sum += qos_samples[curid][5*NDOMAINS + i]
    
        qos_passed += qos_samples[curid][6*NDOMAINS + 0]
        lost_samples.append(qos_samples[curid][6*NDOMAINS + 2])
        ffp_samples.append(qos_samples[curid][6*NDOMAINS + 3])

        numbuckets += 1

        if curid > 0:
            curid -= 1
        else:
            curid = NSAMPLES - 1
        if curid == endat:
            break

    # Collect power data
    socketinfo = SocketData()

    ns_passed = 1       # to prevent zero division
    pmc0_total = 1      # to prevent zero division
    pmc1_total = 1      # to prevent zero division
    pmc2_total = 1      # to prevent zero division
    pmc3_total = 1      # to prevent zero division

    curid = startat
    numbuckets = 0

    # define offsets
    pmc_total_base_addr = 4*NDOMAINS + 3
    rapl_base_addr = 4*NDOMAINS + 3 + 4

    # Note: unrolling from the most recent to the first element 
    tmp_last = power_samples[curid][rapl_base_addr + 1]
    tmp_file.write("------------------------------------\n")
    tmp_i = 0
    tmp_first = 0
    while ns_passed < duration:
        for i in range(0, NDOMAINS):
            if dom_in_use[i]:           # TODO: remove this to show all of them
                socketinfo.domain[i].performance.pmc0 += power_samples[curid][0*NDOMAINS + i]
                socketinfo.domain[i].performance.pmc1 += power_samples[curid][1*NDOMAINS + i]
                socketinfo.domain[i].performance.pmc2 += power_samples[curid][2*NDOMAINS + i]
                socketinfo.domain[i].performance.pmc3 += power_samples[curid][3*NDOMAINS + i]
    
        ns_passed += power_samples[curid][4*NDOMAINS + 0]
        
        socketinfo.total.performance.pmc0 += power_samples[curid][pmc_total_base_addr + 0]
        socketinfo.total.performance.pmc1 += power_samples[curid][pmc_total_base_addr + 1]
        socketinfo.total.performance.pmc2 += power_samples[curid][pmc_total_base_addr + 2]
        socketinfo.total.performance.pmc3 += power_samples[curid][pmc_total_base_addr + 3]
        
        tmp_i += 1
        tmp_file.write("%d) %8d += %8d = %8d - %8d \n" % (tmp_i, socketinfo.total.energy.pkg, (power_samples[curid][rapl_base_addr + 1] - power_samples[curid][rapl_base_addr + 0]), power_samples[curid][rapl_base_addr + 1], power_samples[curid][rapl_base_addr + 0]))
        
        # Note: accumulating incremental values, to avoid samples with RAPL counters == 0
        delta_pkg = (power_samples[curid][rapl_base_addr + 1] - power_samples[curid][rapl_base_addr + 0])
        delta_pp0 = (power_samples[curid][rapl_base_addr + 3] - power_samples[curid][rapl_base_addr + 2])
        delta_pp1 = (power_samples[curid][rapl_base_addr + 5] - power_samples[curid][rapl_base_addr + 4])
        delta_dram = (power_samples[curid][rapl_base_addr + 7] - power_samples[curid][rapl_base_addr + 6])
        
        if delta_pkg > 0:
            socketinfo.total.energy.pkg += delta_pkg
        if delta_pp0 > 0:
            socketinfo.total.energy.pp0 += delta_pp0
        if delta_pp1 > 0:
            socketinfo.total.energy.pp1 += delta_pp1
        if delta_dram > 0:
            socketinfo.total.energy.dram += delta_dram
        
        numbuckets += 1

        tmp_first = power_samples[curid][rapl_base_addr + 0]

        if curid > 0:
            curid -= 1
        else:
            curid = NSAMPLES - 1
        if curid == endat:
            break

    socketinfo.s_passed = float(ns_passed)/(10**9)
    socketinfo.total.energy.pkg = socketinfo.total.energy.pkg*energy_unit
    socketinfo.total.energy.pp0 = socketinfo.total.energy.pp0*energy_unit
    socketinfo.total.energy.pp1 = socketinfo.total.energy.pp1*energy_unit
    socketinfo.total.energy.dram = socketinfo.total.energy.dram*energy_unit

    # tmp_file.write("%8d = %8d - %8d     in: %2.3f s, on: %2.3f s plannes\n" % (tmp_last - tmp_first, tmp_last, tmp_first, socketinfo.s_passed, duration/(10**9)))
    # tmp_file.write("Cumulated: %8d (delta of: %8d)\n" % (socketinfo.total.energy.pkg, socketinfo.total.energy.pkg - (tmp_last - tmp_first)))
    tmp_file.write("------------------------------------\n")

    lostinfo = [min(lost_samples), sum(lost_samples), max(lost_samples)]
    ffpinfo = [min(ffp_samples), sum(ffp_samples), max(ffp_samples)]

    ldoms = []
    for x in range(0, NDOMAINS):
        if dom_in_use[x]:
            ldoms.append(dominfos[x].stats(qos_passed))
        else:
            ldoms.append(0)

    # Extract energy and power consumption data
    socketinfo.total.update_stats(socketinfo.total.performance, socketinfo.total.energy, socketinfo.s_passed)
    for x in range(0, NDOMAINS):
        if dom_in_use[x]:
            socketinfo.domain[x].update_stats(socketinfo.total.performance, socketinfo.total.energy, socketinfo.s_passed)
        else:
            socketinfo.domain[x] = None
    return [ldoms, lostinfo, ffpinfo, socketinfo]

# scale microseconds to milliseconds or seconds as necessary
def time_scale(ns):
    if ns < 1000:
        return "%4.2f ns" % float(ns)
    elif ns < 1000*1000:
        return "%4.2f us" % (float(ns)/10**3)
    elif ns < 10**9:
        return "%4.2f ms" % (float(ns)/10**6)
    else:
        return "%4.2f s" % (float(ns)/10**9)

# scale ops to kops, Mops or Gops as necessary
def ops_scale(ops):
    if ops < 1000:
        return "%4.2f ops" % float(ops)
    elif ops < 1000*1000:
        return "%4.2f kops" % (float(ops)/10**3)
    elif ops < 10**9:
        return "%4.2f Mops" % (float(ops)/10**6)
    else:
        return "%4.2f Gops" % (float(ops)/10**9)


# paint message on curses screen, but detect screen size errors
def display(scr, row, col, str, attr=0):
    try:
        scr.addstr(row, col, str, attr)
    except:
        scr.erase()
        _c.nocbreak()
        scr.keypad(0)
        _c.echo()
        _c.endwin()
        print "Your terminal screen is not big enough; Please resize it."
        print "row=%d, col=%d, str='%s'" % (row, col, str)
        sys.exit(1)


# diplay domain id
def display_domain_id(scr, row, col, dom):
    if dom == IDLE_DOMAIN:
        display(scr, row, col-1, "Idle")
    else:
        display(scr, row, col, "%d" % dom)


# the live monitoring code
def show_livestats(cpu):
    ncpu = 1         # number of cpu's on this platform
    qos_slen = 0     # size of shared data structure, incuding padding
    power_slen = 0   # size of shared data structure, incuding padding - TODO: unused!
    cpu_1sec_usage = 0.0
    cpu_10sec_usage = 0.0
    heartbeat = 1
    global dom_in_use, options, energy_unit
    
    # mmap the (the first chunk of the) file
    shmf_qos = open(SHM_FILE, "r+")
    shm_qos = mmap.mmap(shmf_qos.fileno(), QOS_DATA_SIZE)

    shmf_power = open(SHM_FILE_POWER, "r+")
    shm_power = mmap.mmap(shmf_power.fileno(), POWER_DATA_SIZE)

    # initialize curses
    stdscr = _c.initscr()
    _c.noecho()
    _c.cbreak()

    stdscr.keypad(1)
    stdscr.timeout(1000)
    [maxy, maxx] = stdscr.getmaxyx()
    
    # display in a loop
    while True:

        cpuidx = 0
        while cpuidx < ncpu:

            # calculate offset in mmap file to start from
            idx = cpuidx * qos_slen

            qos_samples = []
            doms = []
            dom_in_use = []
            domain_id = []

            # Unroll all the qos_samples for the current CPU
            for i in range(0, NSAMPLES):
                len = struct.calcsize(ST_QDATA)
                sample = struct.unpack(ST_QDATA, shm_qos[idx:idx+len])
                qos_samples.append(sample)
                idx += len

            for i in range(0, NDOMAINS):
                len = struct.calcsize(ST_DOM_INFO)
                dom = struct.unpack(ST_DOM_INFO, shm_qos[idx:idx+len])
                doms.append(dom)
#               (last_update_time, start_time, runnable_start_time, blocked_start_time,
#                ns_since_boot, ns_oncpu_since_boot, runnable_at_last_update,
#                runnable, in_use, domid, junk, name) = dom
#               dom_in_use.append(in_use)
                dom_in_use.append(dom[8])
                domid = dom[9]
                if domid == 32767 :
                    domid = IDLE_DOMAIN
                domain_id.append(domid)
                idx += len
#            print "dom_in_use(cpu=%d): " % cpuidx, dom_in_use


            len = struct.calcsize("4i")
            oldncpu = ncpu
            (next, ncpu, qos_slen, freq) = struct.unpack("4i", shm_qos[idx:idx+len])
            idx += len

            # xempowerbaked tells us how many cpu's it's got, so re-do
            # the mmap if necessary to get multiple cpu data
            if oldncpu != ncpu:
                shm_qos = mmap.mmap(shmf_qos.fileno(), ncpu*qos_slen)

            # if we've just calculated data for the cpu of interest, then
            # stop examining mmap data and start displaying stuff
            if cpuidx == cpu:
                break

            cpuidx = cpuidx + 1

        # calculate offset in mmap file to start from
        idx = 0
        power_samples = []

        # Unroll all the power_samples for the current CPU
        for i in range(0, NSAMPLES):
            len = struct.calcsize(ST_QDATA_POWER)
            sample = struct.unpack(ST_QDATA_POWER, shm_power[idx:idx+len])
            power_samples.append(sample)
            idx += len

        len = struct.calcsize("2i1d")
        (next, power_slen, energy) = struct.unpack("2i1d", shm_power[idx:idx+len])
        energy_unit = energy
        
        # calculate starting and ending datapoints; never look at "next" since
        # it represents live data that may be in transition. 
        startat = next - 1
        if next + 10 < NSAMPLES:
            endat = next + 10
        else:
            endat = 10

        # get summary over desired interval
        [h1, l1, f1, socketInfo_1s] = summarize(startat, endat, 10**9, qos_samples, power_samples)        # 10^9 ns = 1s
        [h2, l2, f2, socketInfo_10s] = summarize(startat, endat, 10 * 10**9, qos_samples, power_samples)   # 10 * 10^9 ns = 10s

        # the actual display code
        row = 0

        # e.g.: " CPU = 0        Last 10 seconds (0.00%)                              Last 1 second (99.98%) "
        display(stdscr, row, 1, "CPU = %d" % cpu, _c.A_STANDOUT)
        display(stdscr, row, 10, "%sLast 10 seconds (%3.2f%%)%sLast 1 second (%3.2f%%)" % (6*' ', cpu_10sec_usage, 30*' ', cpu_1sec_usage), _c.A_BOLD)
        row +=1

        # e.g.: " =============================================================================================== "
        display(stdscr, row, 1, "%s" % ((maxx-2)*'='))

        total_h1_cpu = 0
        total_h2_cpu = 0

        cpu_1sec_usage = 0.0
        cpu_10sec_usage = 0.0

        for dom in range(0, NDOMAINS):
            if not dom_in_use[dom]:
                continue                    # Skip domains not running

            # Show only domains actually used, but keep idle_domain
            if h1[dom][0][1] > 0 or domain_id[dom] == IDLE_DOMAIN:
                # display gotten
                # e.g.: "  0   9.74 ms     0.97%       286.63 us/ex      15.82 ms    1.58%       354.56 us/ex      Gotten"
                row += 1 
                col = 2
                display_domain_id(stdscr, row, col, domain_id[dom])
                col += 4
                display(stdscr, row, col, "%s" % time_scale(h2[dom][0][0]))
                col += 12
                display(stdscr, row, col, "%3.2f%%" % h2[dom][0][1])
                if dom != IDLE_DOMAIN:
                    cpu_10sec_usage += h2[dom][0][1]
                col += 12
                display(stdscr, row, col, "%s/ex" % time_scale(h2[dom][0][2]))
                col += 18
                display(stdscr, row, col, "%s" % time_scale(h1[dom][0][0]))
                col += 12
                display(stdscr, row, col, "%3.2f%%" % h1[dom][0][1])
                col += 12
                display(stdscr, row, col, "%s/ex" % time_scale(h1[dom][0][2]))
                col += 18
                display(stdscr, row, col, GOTTEN)

                if dom != IDLE_DOMAIN:
                    cpu_1sec_usage = cpu_1sec_usage + h1[dom][0][1]
    
                # display allocated
                if options.allocated:
                    row += 1
                    col = 2
                    display_domain_id(stdscr, row, col, domain_id[dom])
                    col += 28
                    display(stdscr, row, col, "%s/ex" % time_scale(h2[dom][1]))
                    col += 42
                    display(stdscr, row, col, "%s/ex" % time_scale(h1[dom][1]))
                    col += 18
                    display(stdscr, row, col, ALLOCATED)

                # e.g.: "  0   988.61 ms   98.86%      0.00 ns/io        991.65 ms   99.16%      0.00 ns/io        Blocked"
                # display blocked
                if options.blocked:
                    row += 1
                    col = 2
                    display_domain_id(stdscr, row, col, domain_id[dom])
                    col += 4
                    display(stdscr, row, col, "%s" % time_scale(h2[dom][2][0]))
                    col += 12
                    display(stdscr, row, col, "%3.2f%%" % h2[dom][2][1])
                    col += 12
                    display(stdscr, row, col, "%s/io" % time_scale(h2[dom][2][2]))
                    col += 18
                    display(stdscr, row, col, "%s" % time_scale(h1[dom][2][0]))
                    col += 12
                    display(stdscr, row, col, "%3.2f%%" % h1[dom][2][1])
                    col += 12
                    display(stdscr, row, col, "%s/io" % time_scale(h1[dom][2][2]))
                    col += 18
                    display(stdscr, row, col, BLOCKED)

                # e.g.: "  0   108.45 us   0.01%       3.23 us/ex        88.39 us    0.01%       2.98 us/ex        Waited"
                # display waited
                if options.waited:
                    row += 1
                    col = 2
                    display_domain_id(stdscr, row, col, domain_id[dom])
                    col += 4
                    display(stdscr, row, col, "%s" % time_scale(h2[dom][3][0]))
                    col += 12
                    display(stdscr, row, col, "%3.2f%%" % h2[dom][3][1])
                    col += 12
                    display(stdscr, row, col, "%s/ex" % time_scale(h2[dom][3][2]))
                    col += 18
                    display(stdscr, row, col, "%s" % time_scale(h1[dom][3][0]))
                    col += 12
                    display(stdscr, row, col, "%3.2f%%" % h1[dom][3][1])
                    col += 12
                    display(stdscr, row, col, "%s/ex" % time_scale(h1[dom][3][2]))
                    col += 18
                    display(stdscr, row, col, WAITED)

                # display ex count
                if options.excount:
                    row += 1
                    col = 2
                    display_domain_id(stdscr, row, col, domain_id[dom])
                    
                    col += 28
                    display(stdscr, row, col, "%d/s" % h2[dom][4])
                    col += 42
                    display(stdscr, row, col, "%d" % h1[dom][4])
                    col += 18
                    display(stdscr, row, col, EXCOUNT)

                # display io count
                if options.iocount:
                    row += 1
                    col = 2
                    display_domain_id(stdscr, row, col, domain_id[dom])
                    col += 4
                    display(stdscr, row, col, "%d/s" % h2[dom][5][0])
                    col += 24
                    display(stdscr, row, col, "%d/ex" % h2[dom][5][1])
                    col += 18
                    display(stdscr, row, col, "%d" % h1[dom][5][0])
                    col += 24
                    display(stdscr, row, col, "%3.2f/ex" % h1[dom][5][1])
                    col += 18
                    display(stdscr, row, col, IOCOUNT)

            #row += 1
            #stdscr.hline(row, 1, '-', maxx - 2)
            total_h1_cpu += h1[dom][0][1]
            total_h2_cpu += h2[dom][0][1]

        row += 1
        star = heartbeat * '*'
        heartbeat = 1 - heartbeat
        display(stdscr, row, 1, star)
        display(stdscr, row, 2, TOTALS % (total_h2_cpu, total_h1_cpu))
        row += 1
#        display(stdscr, row, 2, 
#                "\tFFP: %d (Min: %d, Max: %d)\t\t\tFFP: %d (Min: %d, Max %d)" % 
#                (math.ceil(f2[1]), f2[0], f2[2], math.ceil(f1[1]), f1[0], f1[2]), _c.A_BOLD)

        row += 2
        # TODO: fix values here
        # e.g.: " Total energy        Last 10 seconds: 0.00 J                              Last 1 second: 0.00 J "
        display(stdscr, row, 1, "Consumption", _c.A_STANDOUT)
        display(stdscr, row, 12, "%sLast 10 seconds: %3.2f J (%3.2f W)%sLast 1 second: %3.2f J (%3.2f W)" % (4*' ', socketInfo_10s.total.energy.pkg, socketInfo_10s.total.power.pkg, 30*' ', socketInfo_1s.total.energy.pkg, socketInfo_10s.total.power.pkg), _c.A_BOLD)
        row +=1

        # e.g.: " =============================================================================================== "
        display(stdscr, row, 1, "%s" % ((maxx-2)*'='))

        row += 1 
        col = 2
        display(stdscr, row, col, "Domain")
        col += 18
        display(stdscr, row, col, "%")
        col += 12
        display(stdscr, row, col, "Energy")
        col += 12
        display(stdscr, row, col, "Power")
        col += 18
        display(stdscr, row, col, "%")
        col += 12
        display(stdscr, row, col, "Energy")
        col += 12
        display(stdscr, row, col, "Power")
        
        #with open(LOG_FILE,'a+') as log_file:
         #   for dom in range(0, NDOMAINS):
               # if not dom_in_use[dom]:
                #    continue              # Skip domains not running
                #row += 1 
                #col = 2
                #display_domain_id(stdscr, row, col, domain_id[dom])
                #col += 18
                #display(stdscr, row, col, "%3.2f%%" % (socketInfo_10s.domain[dom].performance_rel.pmc0*100))
                #col += 12
                #display(stdscr, row, col, "%2.2f J" % socketInfo_10s.domain[dom].energy.pkg)
                #col += 12
                #display(stdscr, row, col, "%2.2f W" % socketInfo_10s.domain[dom].power.pkg)
                #col += 18
                #display(stdscr, row, col, "%3.2f%%" % (socketInfo_1s.domain[dom].performance_rel.pmc0*100))
                #col += 12
                #display(stdscr, row, col, "%2.2f J" % socketInfo_1s.domain[dom].energy.pkg)
                #col += 12
#                display(stdscr, row, col, "%2.2f W" % socketInfo_1s.domain[dom].power.pkg)

#                log_file.write("%s,%s;" % (domain_id[dom],str(socketInfo_1s.domain[dom].performance.pmc0)))
 #           log_file.write("\n")

        if l1[1] > 1 :
            row += 1
            display(stdscr, row, 2, 
                    "\tRecords lost: %d (Min: %d, Max: %d)\t\t\tRecords lost: %d (Min: %d, Max %d)" % 
                    (math.ceil(l2[1]), l2[0], l2[2], math.ceil(l1[1]), l1[0], l1[2]), _c.A_BOLD)

        # grab a char from tty input; exit if interrupt hit
        try:
            c = stdscr.getch()
        except:
            break
        
        # q = quit
        if c == ord('q'):
            break
    
        # c = cycle to a new cpu of interest
        if c == ord('c'):
            cpu = (cpu + 1) % ncpu

        # n/p = cycle to the next/previous CPU
        if c == ord('n'):
            cpu = (cpu + 1) % ncpu
        if c == ord('p'):
            cpu = (cpu - 1) % ncpu

        stdscr.erase()

    _c.nocbreak()
    stdscr.keypad(0)
    _c.echo()
    _c.endwin()
    shm_qos.close()
    shmf_qos.close()
    shm_power.close()
    shmf_power.close()


# simple functions to allow initialization of log files without actually
# physically creating files that are never used; only on the first real
# write does the file get created
class Delayed(file):
    def __init__(self, filename, mode):
        self.filename = filename
        self.saved_mode = mode
        self.delay_data = ""
        self.opened = 0

    def delayed_write(self, str):
        self.delay_data = str

    def write(self, str):
        if not self.opened:
            self.file = open(self.filename, self.saved_mode)
            self.opened = 1
            self.file.write(self.delay_data)
        self.file.write(str)

    def rename(self, name):
        self.filename = name

    def flush(self):
        if  self.opened:
            self.file.flush()

    def close(self):
        if  self.opened:
            self.file.close()
            

# TODO: rewrite all of this
def writelog():
    global options
    global dom_in_use

    ncpu = 1        # number of cpu's
    qos_slen = 0        # size of shared structure inc. padding

    shmf_qos = open(SHM_FILE, "r+")
    shm_qos = mmap.mmap(shmf_qos.fileno(), QOS_DATA_SIZE)

    interval = 0
    curr = last = time.time()
    outfiles = {}
    for dom in range(0, NDOMAINS):
        outfiles[dom] = Delayed("%s-dom%d.log" % (options.prefix, dom), 'w')
        outfiles[dom].delayed_write("# passed cpu dom cpu(tot) cpu(%) cpu/ex allocated/ex blocked(tot) blocked(%) blocked/io waited(tot) waited(%) waited/ex ex/s io(tot) io/ex\n")

    while options.duration == 0 or interval < (options.duration * 1000):
        cpuidx = 0
        while cpuidx < ncpu:

            idx = cpuidx * qos_slen      # offset needed in mmap file

            qos_samples = []
            doms = []
            dom_in_use = []
            domain_id = []

            for i in range(0, NSAMPLES):
                len = struct.calcsize(ST_QDATA)
                sample = struct.unpack(ST_QDATA, shm_qos[idx:idx+len])
                qos_samples.append(sample)
                idx += len

            for i in range(0, NDOMAINS):
                len = struct.calcsize(ST_DOM_INFO)
                dom = struct.unpack(ST_DOM_INFO, shm_qos[idx:idx+len])
#                doms.append(dom)
#               (last_update_time, start_time, runnable_start_time, blocked_start_time,
#                ns_since_boot, ns_oncpu_since_boot, runnable_at_last_update,
#                runnable, in_use, domid, junk, name) = dom
                dom_in_use.append(dom[8])
                domid = dom[9]
                if domid == 32767:
                    domid = IDLE_DOMAIN
                domain_id.append(domid)
                if domid == IDLE_DOMAIN:
                    outfiles[i].rename("%s-idle.log" % options.prefix)
                else:
                    outfiles[i].rename("%s-dom%d.log" % (options.prefix, domid))
                idx += len

            len = struct.calcsize("4i")
            oldncpu = ncpu
            (next, ncpu, qos_slen, freq) = struct.unpack("4i", shm_qos[idx:idx+len])
            idx += len

            if oldncpu != ncpu:
                shm_qos = mmap.mmap(shmf_qos.fileno(), ncpu*qos_slen)

            startat = next - 1
            if next + 10 < NSAMPLES:
                endat = next + 10
            else:
                endat = 10

            [h1,l1, f1] = summarize(startat, endat, options.interval * 10**6, qos_samples, power_samples)
            for dom in range(0, NDOMAINS):
                if not dom_in_use[dom]:
                    continue
                if h1[dom][0][1] > 0 or dom == IDLE_DOMAIN:
                    outfiles[dom].write("%.3f %d %d %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n" %
                                     (interval, cpuidx, domain_id[dom],
                                     h1[dom][0][0], h1[dom][0][1], h1[dom][0][2],
                                     h1[dom][1],
                                     h1[dom][2][0], h1[dom][2][1], h1[dom][2][2],
                                     h1[dom][3][0], h1[dom][3][1], h1[dom][3][2],
                                     h1[dom][4], 
                                     h1[dom][5][0], h1[dom][5][1]))
                    outfiles[dom].flush()
            curr = time.time()
            interval += (curr - last) * 1000
            last = curr
            cpuidx = cpuidx + 1
        time.sleep(options.interval / 1000.0)

    for dom in range(0, NDOMAINS):
        outfiles[dom].close()

# Ensure that the msr module is loaded in the kernel
def load_msr():
    global msr_command
    os.system(msr_command)

# start xempowerbaked
def start_xempowerbaked():
    global options
    global kill_cmd
    global xempowerbaked_cmd

    os.system(kill_cmd)
    os.system(xempowerbaked_cmd + " --ms_per_sample=%d &" %
              options.mspersample)
    time.sleep(1)

# stop xempowerbaked
def stop_xempowerbaked():
    global stop_cmd
    os.system(stop_cmd)

def main():
    global options
    global args
    global domains
    global stop_cmd
    global kill_cmd
    global xempowerbaked_cmd
    global tmp_file

    tmp_file = open("tmp_file.txt", 'w')        # TODO: temp

    if os.uname()[0] == "SunOS":
        xempowerbaked_cmd = "/usr/lib/xempowerbaked"
	stop_cmd = "/usr/bin/pkill -INT -z global xempowerbaked"
	kill_cmd = "/usr/bin/pkill -KILL -z global xempowerbaked"
    else:
        # assumes that xempowerbaked is in your path
        xempowerbaked_cmd = "xempowerbaked"
        stop_cmd = "/usr/bin/pkill -INT xempowerbaked"
        kill_cmd = "/usr/bin/pkill -KILL xempowerbaked"

    parser = setup_cmdline_parser()
    (options, args) = parser.parse_args()

    if len(args):
        parser.error("No parameter required")
    if options.mspersample < 0:
        parser.error("option --ms_per_sample: invalid negative value: '%d'" %
                     options.mspersample)
    # If --ms_per_sample= is too large, no data may be logged.
    if not options.live and options.duration != 0 and \
       options.mspersample > options.duration * 1000:
        parser.error("option --ms_per_sample: too large (> %d ms)" %
                     (options.duration * 1000))
    
    load_msr()
    start_xempowerbaked()
    if options.live:
        show_livestats(options.cpu)
    else:
        try:
            writelog()
        except:
            print 'Quitting.'
    stop_xempowerbaked()
    tmp_file.close()                            # TODO: temp

if __name__ == "__main__":
    main()
